const products = [
    {
        id:1,
        productName: "Empanada",
        price: 2000,
        description: "pasabocas frito relleno de pollo, carne o mixto.",
        q:5
    },
    {   
        id:2,
        productName: "Arepa",
        price: 1500,
        description: "pasabocas de maiz asado.",
        q:5
    },
    {   
        id:3,
        productName: "Sandwich",
        price: 2500,
        description: "pan blanco, jamon y queso. ",
        q:1
    },
    {   
        id:4,
        productName: "Pastel de pollo",
        price: 3000,
        description: "pasabocas horneado con masa de hojaldre y horneado.",
        q:1
    },
    {   
        id:5,
        productName: "Salchipapa",
        price: 5000,
        description: "papas a la francesa y salchicha, salsas al gusto.",
        q:1
    },
    {   
        id:6,
        productName: "Sandwich mamut",
        price: 5000,
        description: "pan baguette, lechuga, queso, jamon, pollo, tomate, cebolla, salsas al gusto.",
        q:1
    },
    {   
        id:7,
        productName: "jugo natural",
        price: 2500,
        description: "jugo en agua.",
        q:1
    },
    {   
        id:8,
        productName: "Botella de agua",
        price: 2500,
        description: "botella de agua mineral de 250ml.",
        q:1
    },
    {   
        id:9,
        productName: "Gaseosas surtidas",
        price: 3000,
        description: "botella de gaseosa plastica de 250ml.",
        q:1
    },
    {   
        id:10,
        productName: "Té",
        price: 3000,
        description: "botella con té de limo o durazno plastica de 250ml.",
        q:1
    }
];


const AllProducts = () => {
    return products;
}

const pushProduct = (productName,price,description) => {
    const valId = products[products.length-1].id+1;
    const newProduct =    {   
        id:valId,
        productName: productName,
        price: price,
        description: description,
        q:1
    }
   products.push(newProduct);
}

module.exports = {AllProducts, pushProduct}

