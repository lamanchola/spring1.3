# PRIMER PROYECTO ACAMICA RESTAURANTE

# PRIMER PROYECTO ACAMICA RESTAURANTE Ajustes 17 de enero

Se solucionaron los errores en la creacion de pedidos y la gestion del sistema desde el perfil de administrador 

Se encontraran las siguientes funciones en la aplicacion:

* Observar todos los usuarios registrados.
* Crear un nuevo usuario.
* Iniciar sesión con el nuevo usuario.
* Ver todos los productos disponibles y sus precios.
* Crear, editar y eliminar productos si es el usuario es Administrador.
* Crear pedidos y editarlos

# Empecemos 

## Instalación de las librerias necesarias 

_Estas instrucciones te permitirán correr el proyecto y realizar las pruebas correspondientes._

1. Descargar el repositorio en este [link](https://gitlab.com/lamanchola/proyecto-sprint-1-acamica.git) e instalar los packages como se muestra a continuación.

```
npm install
```

2. Ejecutar el proyecto con el siguiente comando:

```
node src/index.js
```

3. Dirigirse a la documentación de Swagger en el siguiente [link](https://localhost:5000/api-docs/)


## RUTAS

### USUARIOS

_Para crear un usuario se tendra que llenar todos los datos de este formulario como se muestra en el siguiente ejemplo:_

```
{
    "usuario": "Camila01",
    "nombre": "Camila",
    "apellido": "Manchola",
    "correo": "camila@gmail.com",
    "telefono": "3148769498",
    "direccion": "Cra 7a # 2-99 sur",
    "contraseña": "12345"
}
```
Importante: El rol de los usuarios nuevos siempre va a ser Usuario y no Administrador.

_Para iniciar sesión como nuevo usuario en la ruta "Login", tendrás que utilizar este esquema:_

```
{
    "correo":"camila@gmail.com",
    "contraseña":"12345"
}
```

### PEDIDOS

_Para crear un pedido se tendra que llenar el siguiente esquema:_

```
{
    "nombres":["Empanada", "Coca-cola"],
    "cantidades":[3,1],
    "mediodepago": "Efectivo",
    "estado":"Cerrado"
}
```

Puntos Claves:

* Lo primero es, esta API distingue por id y no por usuario registrado o activo, entonces, para crear un pedido deberás llenar el formulario o body y escribir el id del usuario al que quieras asignarle el pedido, en la ruta de historial se podrán ver los pedidos realizados por usuario, y la ruta para administradores mostrará todos los pedidos sin importar el usurio, los usuarios nuevos podrán ver su id en la ruta de usuarios, ruta donde también se podrá observar el id de los usuarios existentes. 

* Para  el campo "nombres" se puede llenar con tantos nombres de productos como se desee, aunque se cuenta solo con 10 en el sistema, se pueden repetir, siempre y cuando estos existan dentro de la lista de productos, también es importante recalcar que se debe respetar la escritura, cualquier producto escrito de mala manera, hará que el programa presente un error del tipo: _cannot calculated price of undefined_.

* El campo "cantidades" tiene que tener la misma longitud del campo "nombres", es decir, cada producto escrito en el campo "nombres" debe tener su cantidad correspondiente.

* IMPORTANTE: Si el estado del pedido se envía como "cerrado", en la ruta de edición, no se podrá hacer nada, para editar el pedido el estado tiene que decir "abierto".

_Hay dos maneras de ver tus pedidos:_

1. En la ruta de historial/id observarás los pedidos id por id, cabe recordad que el id es el del usuario.

2. En la ruta de pedidos, los administradores podrán observar todos los pedidos hechos por todos los usuarios.

_Hay dos rutas de edición de pedidos:_

1. Para que los usuarios editen, recuerda que se crea pedido por id de usuario y se edita de la misma manera, con el id del usuario.

2. La ruta para que los administradores cambien el estado del pedido de los usuarios.


## Construido con lenguage

* NodeJS
* Express
* Swagger

## Autor

* **Laura Camila Manchola Cocuy** 
